# Rappels sur l'utilisation de git et Gitlab

## Site forge.univ-lyon1.fr

Le site https://forge.univ-lyon1.fr héberge une forge logicielle (Gitlab) qui permet d'héberger des projets, par exemple les TPs de LIFPF.
Il est possible de donner des droits à d'autres utilisateurs, par exemple pour travailler en binôme (dans un projet depuis le menu "Project information > Members").

## Créer un nouveau projet

Un fois connecté(e), il est possible de créer un nouveau projet (bouton bleu à droite).

![gitlab-1-new-project](files/gitlab-1-new-project.png)

Choisir de créer un projet vierge (blank project).

![gitlab-2-blank-project](files/gitlab-2-blank-project.png)

Donner un nom au projet, choisir un répertoire parent (typiquement votre nom d'utilisateur) et laisser cochée la case du README, puis lancer la création via le bouton "Create project".

![gitlab-3-cfg-project](files/gitlab-3-cfg-project.png)

## Récupérer un projet sur sa machine (clone)

Depuis le projet dans le navigateur, cliquer sur le menu du bouton "Clone" et copier l'URL de clone HTTPS.

![gitlab-4-clone-url](files/gitlab-4-clone-url.png)

Depuis le répertoire où on veut placer le dossier du projet, lancer la commande suivante en remplaçant `<url_du_projet>` par la valeur copiée depuis le navigateur:

```shell
git clone <url_du_projet>
```

Cela créée un répertoire ayant le nom du projet.

Certaines commandes `git`, en particulier `clone`, `pull`, `push` et `fetch`, demanderont probablement un login et un mot de passe. Ce sont ceux de votre compte Lyon 1.

## Synchroniser son travail

Remarque: lors de l'utilisation de VSCode, il est conseillé d'ouvrir le répertoire complet du projet et pas seulement un fichier (via la commande `code nom_du_répertoire` ou via le menu "_File > Open folder_").

Avant de commencer à travailler lancer la commande suivante dans le répertoire du projet:

```shell
git pull
```

Cette commande va répcupérer les dernière mise à jour de votre projet et les appliquer dans le répertoire du projet.

On peut alors commencer à travailler sur les fichiers.
Une fois que le travail stabilisé (par exemple à la fin d'une partie d'un TP), on peut _comiter_ les modifications.
Pour cela, on commence par identifier les fichiers à _comiter_, via la commande `git add`. On peut faire autant de git add que l'on souhaite. La commande git status permet de lister les modifications par rapport à la dernière version _comitée_ par exemple:

```shell
❯ git status
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	titi.ml
	toto.ml
	tutu.ml

nothing added to commit but untracked files present (use "git add" to track)

❯ git add titi.ml

❯ git add toto.ml tutu.ml

❯ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   titi.ml
	new file:   toto.ml
	new file:   tutu.ml
```

Une fois les modifications ajoutée, on peut lancer la commande `git commit` en indiquant un message:

```shell
❯ git commit -m "tp7: partie 1.2 terminée"
[main bfec856] tp7: partie 1.2 terminée
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 titi.ml
 create mode 100644 toto.ml
 create mode 100644 tutu.ml
```

**Attention**, les modification ne sont pas encore répercutées sur forge.univ-lyon1.fr. Pour le faire, il faut utiliser `git push`:

```shell
❯ git push
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 10 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 314 bytes | 314.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/lifpf-tp7.git
   17d2e59..bfec856  main -> main
```

Remarque: VSCode intègre des outils graphiques (vue latérale) pour gérer les opérations `add`, `commit`, `pull` et `push`. Il permet également de voir les modifications apportées au fichiers et facilite la résolution des conflits (cf ci-dessous).

![vscode-1-src-ctl-view](files/vscode-1-src-ctl-view.png)

### Conflits

Lors d'un travail à plusieurs, il est possible que le dépôt ait été édité entre le premier `pull` et le `push`. Dans ce cas `git` refusera de faire le `push`. Il faut alors faire un `pull` pour intégrer les modifications distantes avant de faire le `push`.

Si `git` ne parvient pas à fusionner les modifications, il se peut qu'il génère des conflits, visibles via `git status`et dans VSCode (lettre C à côté du fichier).
Dans ce cas, il faut éditer le fichier et rectifier à la main son contenu.
On peut ouvrir la vue latérale `git` dans VSCode et cliquer sur le fichier pour voir les conflits. Pour chaque conflit, VSCode proposera de prendre une des deux version, voir les deux. Souvent, cela ne dispense pas d'aller éditer le code pour le rectifier.

Une fois les fichiers éditer, on les ajoute comme pour une modification usuelle.
Il suffit ensuite de _comiter_ et pousser (`push`) les modifications.
