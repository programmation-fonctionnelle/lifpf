# UE LIFPF INF2030L Programmation Fonctionnelle

## Semestre 2025 Printemps

- [Configuration pour la salle de TP](CONFIGURATION.md)
- [Rappels git et gitlab](gitlab.md)
- [Questions sur le cours](https://forge.univ-lyon1.fr/programmation-fonctionnelle/lifpf/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=Question)

(2024) indique qu'il s'agit des supports de l'an dernier qui sont à mettre à jour

| jour  | heure         | type       | sujet                       | supports / remarques                                                                             |
| ----- | ------------- | ---------- | --------------------------- | ------------------------------------------------------------------------------------------------ |
| 20/01 | 8h            | CM1        | Intro & Lambda-Calcul       | [Diapositives Intro](cm/lifpf-cm0.pdf), [Diapositives Cours](cm/lifpf-cm1.pdf)                   |
|       | 9h45          | TD1 sauf B | Lambda-Calcul               | [Sujet](td/lifpf-td1-enonce.pdf), [corrigé](td/lifpf-td1-correction.pdf)                         |
|       | 11h30         | TD1 Grp B  |                             |                                                                                                  |
| 27/01 | 8h            | CM2        | OCaml                       | [Diapositives](cm/lifpf-cm2.pdf), [Script démos](cm/cm2-demo.md)                                 |
|       | 9h45 ou 11h30 | TP1        | Prise en main               | [Sujet](tp/tp1.md), [corrigé](tp/tp1.ml)                                                         |
| 03/02 | 8h            | TD2        | Lambda-Calcul               | [Sujet](td/lifpf-td2-enonce.pdf), [corrigé](td/lifpf-td2-correction.pdf)                         |
|       | 9h45 ou 11h30 | TP2        | Listes                      | [Sujet](tp/tp2.md), [corrigé](tp/tp2.ml)                                                         |
| 10/02 | 8h            | CM3        | Struct. ind. + types param. | [Diapositives](cm/lifpf-cm3.pdf) [code](cm/cm3-code.ml)                                          |
|       | 9h45 ou 11h30 | TP3        | Arbres                      | [Sujet](tp/tp3.md) [corrigé](tp/tp3.ml)                                                          |
| 17/02 | 8h            | TD3        | Arbres                      | [Sujet](td/lifpf-td3-enonce.pdf), [corrigé](td/lifpf-td3-correction.pdf)                         |
|       | 9h45 ou 11h30 | TP4        | Arbres génériques           | [Sujet](tp/tp4.md), [corrigé](tp/tp4.ml)                                                         |
| 24/02 | 9h45          | Partiel    |                             | Amphi et place communiqué dans tomuss, 1 feuille manuscrite recto/verso autorisée comme document |
| 10/03 | 8h            | CM4        | Ordre sup.                  | (2024) [Diapositives](cm/lifpf-cm4.pdf)                                                          |
|       | 9h45          | TD4        | Ordre sup.                  | (2024) [Sujet](td/lifpf-td4-enonce.pdf)                                                          |
| 17/03 | 8h            | CM5        | Modules                     | (2024) [Diapositives](cm/lifpf-cm5.pdf)                                                          |
|       | 9h45 ou 11h30 | TP5        | Struct. gen.                | (2024) [Sujet](tp/tp5.md)                                                                        |
| 24/03 | 8h            | TD5        | Ordre sup.                  | (2024) [Sujet](td/lifpf-td5-enonce.pdf)                                                          |
|       | 9h45 ou 11h30 | TP6        | Transf. struct.             | (2024) [Sujet](tp/tp6.md)                                                                        |
| 31/03 | 9h45 ou 11h15 | TP7        | App. OCaml (à changer ?)    | (2024) [Sujet](tp/tp7.md)                                                                        |
| 07/04 | 8h            | CM6        | Foncteurs                   | (2024) [Diapositives](cm/lifpf-cm6.pdf), [code](cm/cm6-code.ml)                                  |
|       | 9h45          | TD6        | Foncteurs                   | (2024) [Sujet](td/lifpf-td6-enonce.pdf)                                                          |

### Annales

- [Printemps 2023, session 1](annales/ccf-2023p-s1.pdf), [corrigé](annales/ccf-2023p-s1-corrige.ml)

### Évaluation

- en TP: 30%
- CC intermédiaire (partiel): 30%
- ECA: 40%
